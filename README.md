# Off The Wall - Michael Jackson
Is the best album of all time
## I'ts the best album of all time because
1. Its made by the king of Pop
2. He went **10 for 10**
3. *Rock With You*
---
- Short and to the point , 42 minutes
- No features
- Amazing music videos
> You got me workin' Workin' day and night


![Off The Wall](https://www.fiftiesstore.com/media/catalog/product/cache/8b6431de9410c89d24dfb37cd261db29/b/e/bert_r56776_xxl.jpg)



## Tracklist

| Track Number  | Song Title |
| ------------- | ------------- |
| 1  | Dont Stop 'Til You Get Enough  |
| 2  | Rock With You  |
| 3  | Workin' Day and Night |
| 4  | Get On the Floor  |
| 5  | Off the Wall  |
| 6  | Girlfriend  |
| 7  | She's Out of My Life  |
| 8  | I Can't Help It  |
| 9  | It's The Falling in Love  |
| 10  | Burn This Disco Out  |
